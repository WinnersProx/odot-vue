import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

import Home from './components/Home/Home';
import Login from './components/Login/Login';

const auth = JSON.parse(localStorage.getItem('auth'));

const routes = [
    { path: '/', component: Home, props: { auth }, beforeEnter: (to, from, next) => { if(!auth || auth === {} ) next('/login'); next();
    }},
    { path: '/login', component: Login},
];

const router = new VueRouter({ routes });


export default router;
