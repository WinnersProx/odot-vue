import firebase from '../config/firebase';
import uuid from 'uuid';

const db = firebase.firestore();

export default class TodosService {

    static async addTodo(data) {
        const tId = uuid();
        Object.assign(data, { tId });
        return await db.collection('todos').doc(tId).set({ data });
    }

    static async updateTodo(tId) {
        return await db.collection('todos').doc(tId).update('data.completed', true);
    }

    static async removeTodo(tId) {
        return await db.collection('todos').doc(tId).delete();
    }

    static get todosCollection() {
        return db.collection('todos');
    }

}