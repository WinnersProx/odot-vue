import firebase from 'firebase';

const { 
    VUE_APP_APIKEY, 
    VUE_APP_AUTH_DOMAIN, 
    VUE_APP_DATABASE_URL, 
    VUE_APP_PROJECT_ID, 
    VUE_APP_STBUCKET, 
    VUE_APP_M_SID, 
    VUE_APP_APPID, 
    VUE_APP_MESUREMENT_ID,
} = process.env;

const config = { 
    apiKey: VUE_APP_APIKEY,
    authDomain: VUE_APP_AUTH_DOMAIN,
    databaseURL: VUE_APP_DATABASE_URL,
    projectId: VUE_APP_PROJECT_ID,
    storageBucket:VUE_APP_STBUCKET ,
    messagingSenderId: VUE_APP_M_SID ,
    appId: VUE_APP_APPID,
    measurementId: VUE_APP_MESUREMENT_ID
};

firebase.initializeApp(config);

export default firebase;
