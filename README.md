## ODOT - FRONTEND APP

 This is an awesome task tracker app that helps any user to monitor his tasks.

## Features

- User can login with Gmail
- User can see only his tasks
- User can add a task to his todo list
- User can check a task as completed
- User can delete a task

## How to test this Locally ?

Note that you need to primarily set up development tools as for the below section then go through all of the following steps sequentially.

- Install Node.js
- Setup [Vue js](https://vuejs.org)

1. Clone this repository

2. Install all the dependencies by running the following command in your command line interface

```sh
    $ npm i
```

3. Create your ```.env``` file and configure it regarding ```.envSample``` to setup the needed environment variables making sure you've installed ```env-cmd``` globaly



5. Once you're done, make sure that everything is up-to-date and start enjoying it by running the following command in your command line interface, Sweeet :

```sh 
    $ npm run serve
```

## Tools for Development

Tools used for development of this Application are:

- Library: [Vue js]
- Code Editor/IDE: [VSCode](https://code.visualstudio.com)
- Programming language: [Javascript]
- Storage : Firebase


## Deployment

- [Odot-vue](https://winnersprox.gitlab.io/odot-vue)

## Author

- Vainqueur Bihame

## Acknowlegements

- Awesomity Lab : [Awesomity Lab](https://awesomity.rw)
